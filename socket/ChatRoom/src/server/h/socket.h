#include <sys/socket.h>
#include <netinet/in.h>

#ifndef SOCKET_H
#define SOCKET_H

void mybzero(struct sockaddr_in *sin);
int mysocket(void);
void mybind(int lfd,struct sockaddr_in *sin);
void mylisten(int lfd);
int myaccept(int lfd,struct sockaddr_in *cin,socklen_t * addr_len);
void mywrite(int sfd, cr_chat_inf* temp);

#endif