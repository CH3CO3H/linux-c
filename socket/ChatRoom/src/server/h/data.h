#ifndef DATA_H
#define DATA_H

#define PORT 64531
#define MAXSIZE 100

struct cr_chat_inf {
	char name[16];
	char msg[256];
};

typedef struct cr_chat_inf cr_chat_inf;

#endif
