#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/select.h>
#include "data.h"
#include "socket.h"

void display(cr_chat_inf temp) {
	printf("name = %s\n",temp.name);
	printf("msg = %s\n",temp.msg);
}

int main(void) {
	char *errmsg;
	int lfd;
	int cfd;
	int rdy;
	struct sockaddr_in sin;
	struct sockaddr_in cin;
	int client[FD_SETSIZE];
	int maxi;
	int maxfd;
	fd_set rset;
	fd_set allset;
	socklen_t addr_len;
	int i;
	int n;
	int len;
	int opt = 1;
	mybzero(&sin);
	lfd = mysocket();
	setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	mybind(lfd,&sin);
	mylisten(lfd);
	printf("Waiting connection...\n");
	maxfd = lfd;
	maxi = -1;
	for(size_t i = 0;i < FD_SETSIZE;i++) {
		client[i] = -1;
	}
	FD_ZERO(&allset);
	FD_SET(lfd,&allset);
	while(1) {
		rset = allset;
		rdy = select(maxfd + 1, &rset, NULL, NULL, NULL);
		if (rdy<0) {
			perror("select()");
			exit(1);
		}
		if(FD_ISSET(lfd, &rset)) {
			addr_len = sizeof(sin);
			cfd = myaccept(lfd,&cin,&addr_len);
			for(i = 0; i<FD_SETSIZE; i++) {
				if(client[i] <= 0) {
					client[i] = cfd;
					break;
				}
			}
			if(i == FD_SETSIZE) {
				printf("too many clients");
				exit(1);
			}
			FD_SET(cfd, &allset);
			if(cfd > maxfd) {
				maxfd = cfd;
			}
			if(i > maxi) {
				maxi = i;
			}
			if(--rdy <= 0) {
				continue;
			}
		}
		for(size_t i=0;i<maxfd;i++) {
			int sfd;
			if((sfd = client[i]) < 0) continue;
			if(FD_ISSET(sfd, &rset)) {
				cr_chat_inf temp;
				char buf[MAXSIZE];
				printf("Recevied msg from client: %d.\n",sfd);
				n = read(sfd, buf, sizeof(buf));
				if(n == 0) {
					printf("Client: %d leave.\n",sfd);
					fflush(stdout);
					close(sfd);
					FD_CLR(sfd, &allset);
					client[i] = -1;
				} else {
					// display(temp);
					char addr_p[20];
					inet_ntop(AF_INET, &cin.sin_addr, addr_p, sizeof(addr_p));
					addr_p[strlen(addr_p)] = '\0';
					printf("Client %s:%d\n", addr_p, ntohs(cin.sin_port));
					printf("Receive: %s\n", buf);
					for (size_t j=0;j<maxfd;j++) {
						if (client[j]<0) continue;
						write(client[j], buf, MAXSIZE);
					}
					printf("Send!\n");
					memset(&temp,0,sizeof(cr_chat_inf));
					memset(buf, 0, MAXSIZE);
				}
				if(--rdy <= 0) {
					break;
				}
			}
		}
	}
	close(lfd);
	return 0;
}
