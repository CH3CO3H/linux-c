#include "data.h"
#include "socket.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>

void mybzero(struct sockaddr_in *sin) {
	bzero(sin,sizeof(struct sockaddr_in));
	sin->sin_family=AF_INET;
	sin->sin_addr.s_addr=htonl(INADDR_ANY);
	sin->sin_port=htons(PORT);
}

int mysocket(void) {
	int lfd;
	if((lfd = socket(AF_INET,SOCK_STREAM,0)) == -1) {
		fprintf(stderr,"Socket error:%s\n\a",strerror(errno));
		exit(1);
	}
	return lfd;
}

void mybind(int lfd,struct sockaddr_in *sin) {
	if(bind(lfd,(struct sockaddr *)(sin),sizeof(struct sockaddr))==-1) {
		fprintf(stderr,"Bind error:%s\n\a",strerror(errno));
		exit(1);
	}
}

void mylisten(int lfd) {
	if(listen(lfd,20)==-1) {
		fprintf(stderr,"Listen error:%s\n\a",strerror(errno));
		exit(1);
	}
}

int myaccept(int lfd,struct sockaddr_in *cin,socklen_t * addr_len)
{
	int cfd;
	if((cfd=accept(lfd,(struct sockaddr *)cin,addr_len))==-1)
	{
		fprintf(stderr,"Accept error:%s\n\a",strerror(errno));
		exit(1);
	}
	return cfd;
}

void mywrite(int sfd, cr_chat_inf* temp)
{
	int num;
	if((num = write(sfd, temp, sizeof(cr_chat_inf))) == -1) {
		printf("send error!\n");
	}
}
