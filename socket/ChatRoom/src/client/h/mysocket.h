#include <netinet/in.h>
#include <sys/socket.h>

#ifndef MY_SOCKET_H
#define MY_SOCKET_H

int mysocket();
void mybezero(struct sockaddr_in *server_addr, struct in_addr* ip);
void myconnect(int sockfd,struct sockaddr_in *server_addr);

#endif