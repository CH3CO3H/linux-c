#ifndef __DATA__
#define __DATA__

#define MAXSIZE 128

#define PORT 64531

struct cr_chat_inf {
	char name[16];
	char msg[256];
};

typedef struct cr_chat_inf cr_chat_inf;

#endif
