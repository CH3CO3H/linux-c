#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include "data.h"
#include "mysocket.h"
#include <pthread.h>

static void* recv_msg(void* arg);

int main(int argc, char *argv[]) {
	int sockfd;
	pthread_t id;
	struct sockaddr_in server_addr;
	struct in_addr ip;
	// cr_chat_inf cinf;
	if(argc != 2) {
		fprintf(stderr,"Usage:%s <hostname> \n",argv[0]);
		return 0;
	}
	inet_aton(argv[1], &ip);
	sockfd = mysocket();
	mybezero(&server_addr, &ip);
	myconnect(sockfd,&server_addr);
	// printf("input your name:\n");
	pthread_create(&id, NULL, recv_msg, (void *)(&sockfd));
	char buf[MAXSIZE];
	while (fgets(buf, MAXSIZE, stdin)) {
		write(sockfd, buf, strlen(buf));
	}
	close(sockfd);
	return 0; 
} 

static void* recv_msg(void* arg) {
	cr_chat_inf msg;
	char buf[MAXSIZE];
	int sockfd=*(int*)arg;
	ssize_t n;
	while (1) {
		if (n=read(sockfd, buf, MAXSIZE)) {
			// printf("%s:\n\t%s\n", msg.name, msg.msg);
			printf("Receive from server: %s\n", buf);
		} else {
			printf("server closed\n");
			exit(0);
		}
	}
}