#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <arpa/inet.h>
#include "mysocket.h"
#include "data.h"

int mysocket(void) {
	int  sockfd;
	if((sockfd = socket(AF_INET,SOCK_STREAM,0)) == -1) {
		fprintf(stderr,"Socket error:%s\n\a",strerror(errno));
		exit(1);
	}
	return sockfd;
}

struct sockaddr_in server_addr;
struct hostent *host;

void mybezero(struct sockaddr_in *server_addr, struct in_addr* ip) {
	bzero(server_addr,sizeof(struct sockaddr_in));
	server_addr->sin_family=AF_INET;
	server_addr->sin_port=htons(PORT);
	server_addr->sin_addr = *ip;
}
void myconnect(int sockfd,struct sockaddr_in *server_addr)
{
	if(connect(sockfd,(struct sockaddr *)(server_addr),sizeof(struct sockaddr))==-1) 
	{ 
		fprintf(stderr,"Connect Error:%s\a\n",strerror(errno)); 
		exit(1); 
	}
}
