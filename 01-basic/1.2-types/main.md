# 数据类型

每种编程语言都有其表示数据的方式，即数据类型。

C 语言中的数据类型分为两大类：整数和浮点数。整数分有符号和无符号；浮点数分实数和复数。几个基本的数据类型如下表所示。

| 类型           | 释义         |
| -------------- | ------------ |
| int            | 整型         |
| char           | 字符型       |
| double         | 双精度浮点数 |
| double complex | 复数         |

字符型可视为整数类型，数值是 ASCII 码表中对应的值。一般情况下不要用字符类型的值参与算数运算。查看 ASCII 码表，可在终端键入

```
$ man ascii
```

完整的数据类型如下

| class          | class      | systematic name     | range                |
| -------------- | ---------- | ------------------- | -------------------- |
| integers       | unsigned   | unsigned char       | [0, UCHAR_MAX]       |
|                |            | unsigned short      | [0, USHRT_MAX]       |
|                |            | unsigned int        | [0, UINT_MAX]        |
|                |            | unsigned long       | [0, ULONG_MAX]       |
|                |            | unsigned long long  |                      |
|                | [un]signed | char                |                      |
|                | signed     | signed char         |                      |
|                |            | signed short        | [SHRT_MIN, SHRT_MAX] |
|                |            | signed int          | [INT_MIN, INT_MAX]   |
|                |            | singed long         | [LONG_MIN, LONG_MAX] |
|                |            | singed long long    |                      |
| floating point | real       | float               | [FLT_MIN, FLT_MAX]   |
|                |            | double              | [DBL_MIN, DBL_MAX]   |
|                |            | long double         |                      |
|                | complex    | float complex       | -                    |
|                |            | double complex      | -                    |
|                |            | long double complex | -                    |